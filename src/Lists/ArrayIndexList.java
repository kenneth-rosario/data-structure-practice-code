package Lists;

public class ArrayIndexList<E> implements IndexList<E> {

	private static final int  INIT_CAP = 3;
	private E[] elements;
	private int size;
	
	
	@SuppressWarnings("unchecked")
	public ArrayIndexList(){
		elements = (E[]) new Object[INIT_CAP];
		size = 0;
	}
	
	public ArrayIndexList(E[] array) {
		if(array.length >= INIT_CAP) {
			elements = (E[]) new Object [array.length*2];
			for (int i = 0; i < array.length; i++) {
				elements[i] = array[i];
				size = array.length;
			}
		}
	}
	
	public E get(int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		return elements[index];
	}

	public E remove(int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		E etr = elements[index];
		for(int i = index; i < size-1; i++) {
			elements[i] = elements[i + 1];
		}
		elements[size-1] = null;
		size--;
		if(elements.length/size == 4 && elements.length > 10) {
			changeCapacity(elements.length/2);
		}
		return etr;
	}

	public void set(int index, E element) {
		// TODO Auto-generated method stub
		if(index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		elements[index] = element;
	}

	public void add(E element, int index) {
		// TODO Auto-generated method stub
		if(size == elements.length) {
			changeCapacity(size*2);
		}
		for(int i = size; i > index; i--) {
			elements[i] = elements[i-1];
		}
		size++;
		elements[index] = element;
	}

	public void add(E element) {
		// TODO Auto-generated method stub
		add(element, size);
	}

	
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}
	
	private void changeCapacity(int i) {
		E[] newArray = (E[]) new Object[i];
		for(int j = 0; j < size; j++) {
			newArray[j] = this.get(j);
		}
		elements = newArray.clone();
	}
	
	@Override
	public String toString() {
		String toReturn = "";
		for(int i = 0; i < size; i++) {
			toReturn += String.valueOf(i) + ": " + String.valueOf(get(i)) + "\n";
		}
		return toReturn;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}
	
	 public Object clone() throws CloneNotSupportedException{
		 return null;
	 }

	
}
