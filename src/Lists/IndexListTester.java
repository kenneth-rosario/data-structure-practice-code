package Lists;

public class IndexListTester {

	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		IndexList<Integer> tester = new DLIndexList<Integer>();  
		
		tester.add(1, 0);
		tester.add(2, 0);
		tester.add(3,1);
		tester.add(5, 3);
		tester.add(54,3);
		System.out.println(tester);
		tester.remove(3);
		tester.set(1, 99);
		System.out.println(tester);
		IndexList<Integer> tester2 = (IndexList<Integer>) tester.clone();
		tester2.remove(0);
		System.out.println(tester);
		System.out.println(tester2);
	}

}
