package Lists;

public class DLIndexList<E> implements IndexList<E>{
	Node<E> head = new Node<E>();
	Node<E> trail = new Node<E>();
	int size = 0;
	private static class Node<E>{
		private E element = null;
		private Node<E> next = null;
		private Node<E> prev = null;
		
		public Node() {
			
		}
		
		public Node(E e) {
			element = e;
		}
		
		public E getElement() {
			return element;
		}
		
		public void setElement(E e) {
			element = e;
		}
		
		public void setNext(Node<E> e) {
			next = e;
		}
		
		public void setPrev(Node<E> e) {
			prev = e;
		}
		
		public Node<E> getNext(){
			return next;
		}
		
		public Node<E> getPrev(){
			return prev;
		}
		
	}
	
	public DLIndexList(){
		head.setNext(trail);
		trail.setPrev(head);
	}
	
	private Node<E> findNode(int index){
		if (index > size/2) {  // Search from the trail back in order to find it easierly
			int counter = size;
			Node<E> p = trail;
			while(counter > index) {
				p = p.getPrev();
				counter --;
			}
			return p;
		}else { // search from start up;
			int counter = -1;
			Node<E> p = head;
			while(counter < index) {
				p = p.getNext();
				counter ++;
			}
			return p;
		}
	}
	
	@Override
	public E get(int index) { 
		// TODO Auto-generated method stub
		if(index < 0 || index >= size) throw new IndexOutOfBoundsException("Invalid Index");
		if(size == 0) return null;
		return findNode(index).getElement();
	}

	@Override
	public E remove(int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index >= size ) throw new IndexOutOfBoundsException("Invalid Index");
		if(size == 0) return null;
		Node<E> etr = findNode(index);
		etr.getPrev().setNext(etr.getNext());
		etr.getNext().setPrev(etr.getPrev());
		size --;
		return etr.getElement();
	}

	@Override
	public void set(int index, E element) {
		// TODO Auto-generated method stub
		Node<E> etr = findNode(index);
		Node<E> newerNode = new Node<E>(element);
		etr.getPrev().setNext(newerNode);newerNode.setPrev(etr.getPrev());
		etr.getNext().setPrev(newerNode);newerNode.setNext(etr.getNext());
	}

	@Override
	public void add(E element, int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index > size) throw new IndexOutOfBoundsException("Invalid Index"); 
		Node<E> newerNode = new Node<E>(element);
		if(size == 0) {
			newerNode.setPrev(head);
			newerNode.setNext(trail);
			head.setNext(newerNode); trail.setPrev(newerNode);
			size ++;
		}else {
			Node<E> toMove = findNode(index);
			toMove.getPrev().setNext(newerNode);
			newerNode.setPrev(toMove.getPrev());newerNode.setNext(toMove);
			toMove.setPrev(newerNode);
			size++;
		}
	}

	@Override
	public void add(E element) {
		// TODO Auto-generated method stub
		add(element, 0);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}
	
	@Override
	public String toString() {
		String tr = "";
		int counter = 0;
		if(size == 0) return null;
		Node<E> p = head.getNext();
		while(counter < size) {
			tr+=String.valueOf(counter) + " :" + String.valueOf(p.getElement()) + "\n";
			p = p.getNext();
			counter++;
		}
		return tr;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		DLIndexList<E> tr = (DLIndexList<E>) super.clone();
		if(size == 0) return tr;
		tr.head = new Node<E>(null);
		tr.trail = new Node<E>(null);
		Node<E> p = tr.head; Node<E> p2 = this.head;
		while ( p2.getNext() != null ) {
			Node<E> newerNode = new Node<E>(p2.getNext().getElement());
			p.setNext(newerNode);
			newerNode.setPrev(p);  
			p = p.getNext(); p2 = p2.getNext();
		}
		tr.trail = p;
		return tr;
	}
	

}
