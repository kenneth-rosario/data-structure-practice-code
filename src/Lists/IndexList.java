package Lists;

public interface IndexList <E> extends Cloneable {
	
	public E get (int index);
	public E remove(int index);
	public void set(int index, E element);
	public void add (E element, int index);
	public void add( E element );
	public int size();
	public boolean isEmpty();
	public Object clone() throws CloneNotSupportedException;
	
	
}
