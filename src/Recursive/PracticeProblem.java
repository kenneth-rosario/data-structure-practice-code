
package Recursive;

public class PracticeProblem {
	
	/**
	 * @param arr array to put even numbers before odds 
	 */
	public static void evenBeforeOdds(int[] arr) {
		recEvenBeforeOdd(arr, 0, 0, false);
	}
	
	private static void recEvenBeforeOdd(int[] arr, int toPlace, int counter, boolean canSwap) {
		if(counter == arr.length) {
			return;
			// if there is no more elments end
		}
		if(arr[counter]%2 == 0) {
			if(canSwap) {
				// if you found an even number and you can swap 
				swap(toPlace, counter, arr);
				// leave the counter where it is since the new element in that position is odd so we must verify it
				recEvenBeforeOdd(arr, toPlace + 1, counter, false);
			}else {
				// if even and cant swap the even number is in its corrct position
				recEvenBeforeOdd(arr, toPlace + 1, counter + 1 , false);
			}
		}
		else {
			if(canSwap) {
				// if it is odd and you can swap, in other words another odd was found before, leave the toPlace as is and continue
				recEvenBeforeOdd(arr,toPlace, counter + 1, canSwap);
			}else {
				// if odd is found place the toPLaceCounter to this position and enable swapping
				recEvenBeforeOdd(arr, counter, counter + 1, true);
			}
		}
	}
	
	private static void swap(int i, int j, int[] arr) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	/**
	 	Given an unsorted array, A, of integers and an integer k, describe a recursive
		algorithm for rearranging the elements in A so that all elements less than or equal
		to k come before any elements larger than k. What is the running time of your
		algorithm on an array of n values?
	 */
	
	public static void valuesSmallerThenKLeft(int[] arr, int k) {
		auxLessThanK(arr, k, 0, arr.length - 1);
	}
	
	private static void auxLessThanK(int[] arr, int k, int toPlace, int counter) {
		// Complexity O(n) 
		if(counter < 0 || toPlace > counter) {
			return;
		}
		if(arr[counter] > k) {
			auxLessThanK(arr, k, toPlace, counter-1);
		}
		else {
			swap(toPlace, counter, arr);
			auxLessThanK(arr, k, toPlace + 1, counter);
		}
	}
	
	private static int powerSum(int p, int n, int[] arr, int index) {
		
		if(p == 0) {
			return 1;
		}
		
		if(index >= arr.length) {
			return 0;
		}
		
		return powerSum(p, n, arr, index + 1) + powerSum(p - (int) Math.pow(arr[index], n), n, arr, index+1);
		
	}
	
	
	private static Integer powerSum(int p, int n) {
		double Power = 1/(double)n;
		int toP =  (int) Math.pow(p, Power);
		int[] a = new int[toP];
		
		for (int i = 1; i <= toP; i++) {
			a[i - 1] = i;
		}
		
		return powerSum(p, n, a, 0);
		
	}
	/**
	 	Describe a recursive algorithm that will check if an array A of integers contains
		an integer A[i] that is the sum of two integers that appear earlier in A, that is, such
		that A[i] = A[j]+A[k] for j,k < i.
	 */
	
	
	
	public static void main(String[] args) {
		System.out.println(powerSum(100, 2));
//		System.out.println(Math.pow(10, 0.5));
	}
	
}
