package Trees;

import java.util.ArrayList;

public class ArrayBinaryTree<E> extends AbstractBinaryTree<E>{

	ArrayList<E> arr = new ArrayList<E>();
	private class BNode<E> implements Position<E>{
		int index;
		
		@Override
		public E getElement() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public BNode(int index){
			this.index = index;
		}
		
		public int index() {
			return index;
		}
	}
	
	private BNode<E> validate(Position<E> p) {
		if(!(p instanceof BNode)) {
			throw new IllegalArgumentException("Invalid Position");
		}
		return (BNode<E>) p;
	}
	
	@Override
	public Position<E> left(Position<E> p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position<E> addLeft(Position<E> p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position<E> right(Position<E> p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position<E> addRight(Position<E> p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position<E> parent(Position<E> p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position<E> root() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BinaryTree<E> generateCompleteTree() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}
