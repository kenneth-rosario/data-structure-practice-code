package Trees;

import java.util.Iterator;

public interface Tree <E> extends Iterable<E>{
	Position<E> root();
	Position<E> parent(Position<E> p);
	Integer numChildren(Position<E> p);
	Iterable<Position<E>> children(Position<E> p);
	void remove(Position<E> p);
	Position<E> addRoot(E e);
	boolean isInternal(Position<E> p);
	boolean isExternal(Position<E> p);
	boolean isRoot(Position<E> p);
	Position<E> addChild(Position<E> p, E e);
	Integer size();
	boolean isEmpty();
	Iterator<Position<E>> positions();
}
