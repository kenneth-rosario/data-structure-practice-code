package Trees;

public interface BinaryTree<E> {
	
	Position<E> left(Position<E> p);
	Position<E> addLeft(Position<E> p);
	Position<E> right(Position<E> p);
	Position<E> addRight(Position<E> p);
	Position<E> parent(Position<E> p);
	Position<E> root();
	/**
	 * 
	 * @return Generates Complete Binary tree using preorder order 
	 */
	BinaryTree<E> generateCompleteTree();
	boolean isEmpty();
	int size();
	boolean hasLeft(Position<E> p);
	boolean hasRight(Position<E> p);
	boolean isRoot(Position<E> p);
}
