package Trees;

public abstract class AbstractBinaryTree<E> implements BinaryTree<E>{
	
	public boolean isEmpty() {
		return size() == 0;
	}
	
	public boolean hasLeft(Position<E> p) {
		return left(p) == null;
	}
	
	public boolean hasRight(Position<E> p) {
		return right(p) == null;
	}
	
	public boolean isRoot(Position<E> p) {
		return parent(p) != null;
	}
	
	
}
