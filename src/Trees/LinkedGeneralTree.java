package Trees;

import java.util.Iterator;

import PositionalList.Position;
import PositionalList.PositionalList;

public class LinkedGeneralTree<E> extends AbstractTree<E>{
	
	private Node<E> root;
	private Integer size;
	
	public LinkedGeneralTree() {
		root = null;
		size = 0;
	}
	
	public LinkedGeneralTree(E p) {
		root = new Node<E>(p, null);
		size = 0;
	}
	
	private Node<E> validate(Trees.Position<E> p){
		if(!(p instanceof Node)) {
			throw new IllegalArgumentException("Position is not valid");
		}
		Node<E> np = ((Node<E>)p);
		if(np.getParent() == np) {
			throw new IllegalArgumentException("Position is not part of this tree");
		}
		return np;
	}
	
	@Override
	public Trees.Position<E> root() {
		// TODO Auto-generated method stub
		return root;
	}

	@Override
	public Trees.Position<E> parent(Trees.Position<E> p) {
		// TODO Auto-generated method stub
		Node<E> np = validate(p);
		return np.getParent();
	}

	@Override
	public Integer numChildren(Trees.Position<E> p) {
		// TODO Auto-generated method stub
		return ((PositionalList) children(p)).size();
	}

	@Override
	public Integer size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public Iterator<Trees.Position<E>> positions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Trees.Position<E>> children(Trees.Position<E> p) {
		// TODO Auto-generated method stub
		Node<E> np = validate(p);
		return np.getChildren();
	}

	@Override
	public void remove(Trees.Position<E> p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Trees.Position<E> addRoot(E e) {
		// TODO Auto-generated method stub
		if(root == null) {
			root = new Node<E>(e, null); size++;
			return root;
		}
		size++;
		throw new IllegalStateException("Tree must be empty in order to add a root");
		
	}
	
	
	
	@Override
	public Trees.Position<E> addChild(Trees.Position<E> p, E e) {
		// TODO Auto-generated method stub
		Node<E> np = validate(p);
		PositionalList<Trees.Position<E>> children = np.getChildren();
	    Position<Trees.Position<E>> toReturn = children.addLast(new Node<E>(e, np));
	    size++;
		return children.last().getElement();

	}
}
