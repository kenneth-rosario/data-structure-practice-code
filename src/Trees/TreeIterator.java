package Trees;

import java.util.Iterator;
import java.util.NoSuchElementException;

import Stacks.ArrayStack;

public class TreeIterator<E> implements Iterator<E>{
	
	ArrayStack<Position<E>> s = new ArrayStack<>();
	Tree<E> t;
	
	public TreeIterator(Tree<E> t) {
		this.t = t;
		s.push(t.root());
	}
	
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return !s.isEmpty();
	}

	@Override
	public E next() {
		// TODO Auto-generated method stub
		if(!hasNext()) {
			throw new NoSuchElementException("Cannot no other values"); 
		}
		
		Position<E> current = s.pop();
		for(Position<E> p: t.children(current)) {
			s.push(p);
		}
		
		return current.getElement();
	}

}
