package Trees;

import PositionalList.DoublyLinkedPositionalList;
import PositionalList.PositionalList;

public class Node<E> implements Position<E>{
	private E element;
	private PositionalList<Position<E>> children;
	private Node<E> Parent;
	
	Node(E elm, Node<E> parent){
		element = elm;
		Parent = parent;
		children = new DoublyLinkedPositionalList<Position<E>>();
	}
	
	@Override
	public E getElement() {
		// TODO Auto-generated method stub
		return element;
	}
	
	public PositionalList<Position<E>> getChildren() {
		return children;
	}
	
	public void discard() {
		Parent = this;
		children = null;
		element = null;
	}
	
	public void setElement(E e) {
		element = e;
	}
	
	public void setParent(Node<E> newP) {
		Parent = newP;
	}
	
	public Node<E> getParent(){
		return Parent;
	}
	
}
