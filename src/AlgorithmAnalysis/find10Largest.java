package AlgorithmAnalysis;

public class find10Largest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] tester = {15, 2, 10, 12, 18, 20, 50, 55, 60, 80, 5, 22, 42, 120, 150, 15, 77, 69, 82, 45, 102, 110, 142};
		int[] largest = find_10_largest(tester);
		printArray(largest);
	}
	
	private static int[] find_10_largest(int [] n) {
		int[] largest = new int[10];
		int smallest = 0;
		largest[0] = n[0];
		for(int i = 1; i < n.length; i++) {
			if(i < 10) {
				if(n[i] < largest[smallest]) {
					smallest = i;
				}
				largest[i] = n[i];
			}else if(n[i] > largest[smallest]){
				largest[smallest] = n[i];
				smallest = findSmallest(largest);
			}
		}
		return largest;
	}
	
	private static int findSmallest(int[] n) {
		int smallest = 0;
		for(int i = 1; i < n.length; i++) {
			if(n[i] < n[smallest]) {
				smallest = i;
			}
		}
		return smallest;
	}

	private static void printArray(int[] n){
		String a = "";
		for(int i = 0 ; i < n.length; i++) {
			a+= n[i] + ", ";
		}
		System.out.println(a);
	}
}
