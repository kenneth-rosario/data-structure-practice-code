package AlgorithmAnalysis;

import java.util.Arrays;
import java.util.Random;
/**
 * 
 * @author Kenneth J. Rosario Acevedo
 *	 
 *	Implement them using Comparators and the Generic Framework for Practice
 */
public class Sorts {
	
	/**
	 * 
	 * HEAPSORT
	 * PROS: Consistent O(nlogn) running time
	 */
	public static void heapSort(int[] elements) {
		heapify(elements, elements.length); // Apply the bottom up heap construction
		int j = 0; 
		while(j < elements.length) {
			swap(0, elements.length- j - 1 , elements); // first swap the first and the last element since the first one is the biggest
			j++; // limit the tree to exclude the last position since it is already sorted
			downheap(0, elements, elements.length - j ); // downheap the root to its correct place 
		} // continue the same process swaping the first element to the last
	}
	
	private static void upheap(int j, int[] arr) {
		while(j > 0) {
			int parent = (j-1)/2;
			if(arr[parent] > arr[j]) break;
			swap(j, parent, arr);
			j = parent;
		}
	}
	
	private static void downheap(int j, int[] arr, int to) {
		while(j < to ) { // while in legal index
			int maxChild = -1 ; // initialize maxChild
			if(2*j + 1 < to) { // if it has a left child set it to that by default
				maxChild = 2*j + 1;
			}
			if(2*j+2 < to) { // if it has a right child
				int right = 2*j+2;
				if(arr[right] > arr[2*j + 1]){  // verify if right child is bigger than left child
					maxChild = right; // if so change the maxchild to be the right child
				}
			}
			if(maxChild < 0) { 
				break;
			}
			if(arr[maxChild] < arr[j]) break; // if the max child is smaller than the parent then the parent is in the correct place
			swap(j, maxChild, arr); // if not we bubble down the parent
			j = maxChild; // and continue where the paren is right now
		}
	}
	

	// Bottom up heap construction 
	private static void heapify(int arr[], int to) {
		for(int i=(arr.length-1)/2 ; i >= 0; i --) {
			downheap(i, arr, arr.length);
		}
	}
	
	// end of heap sort
	
	private static void swap(int j, int i, int[]arr) {
		int temp = arr[j];
		arr[j] = arr[i];
		arr[i] = temp;
	}
	
	/**
	 * 
	 * INSERTIONSORT
	 * PRO: Simple sorting implementation
	 * CONS: O(n^2) running time
	 */
	public static void insertionSort(int[] elements) {
		//Assuming list is not empty
		for(int i = 0; i < elements.length; i++) {
			int key = elements[i];
			int j = i-1;
			
			while(j > -1 && elements[j] > key) {
				elements[j+1] = elements[j];
				j--;
			}
			
			elements[j+1] = key;
					
		}
	}
	 
	/**
	 * 
	 * QUICK SORT
	 * PROS:
	 * 	Average Time: O(nlogn), this scenario can happen more often by selecting a random pivot
	 * CONS:
	 * 	Worst Case Time: If a bad pivot is selected the running time will be O(n^2)
	 * 
	 */
	static void quicksort(int[] arr) {
		Random rand = new Random();
		quicksort(arr, 0, arr.length-1, rand);
	}
	
	private static void quicksort(int[] arr, int low, int high, Random rand) {
		
		if(low < high) {
			int t = low + rand.nextInt(high - low); // randomized pivot index
			int pivot = arr[t]; // take pivot to be random index of the array
			int index = partition(arr, low, high, pivot); // partition array all elements to the left are smaller than index and all elements to the right are bigger
				quicksort(arr, low, index-1, rand);// quicksort the left side 
				quicksort(arr, index, high, rand);// quicksort the right side from index because pivot was not an actualposition in array
		}
		
	}
	
    private static int partition(int[] arr, int left, int right, int pivot) {
		
    	while(left <= right) { // set to pointers, one from left side and one from right side
    		while(arr[left] < pivot) left++; // keep moving until you find an element that >= to the pivot 
    		while(arr[right] > pivot) right--; // keep moving towards the left until you find an element that is <= to the pivot
    		if(left <= right) { // if the pointers are still valid
    			swap(left, right, arr); // swap the left element with the right since this belong to the opposite side
    			left ++; // continue moving towards the right
    			right --; // continue moving towards the left
    		}
    	}
		
		return left; // all elements before left are now smaller than the pivot and all the elements from left to the end are bigger than pivot
	}
    
    
    /**
     * 
     * MERGE SORT
     * Easy to visualize and to understand 
     * PROS: O(nlogn) consistant execution time
     * CONS: O(n) memory consistant time
     * 
     */
    static void mergesort(int[] arr) {
    	mergesort(0, arr.length, arr);
	}
    
    private static void mergesort(int low, int high, int[] arr) {
    	if(arr.length < 2) { return; } // if the array is of size 1 or empty, it is already sorted
    	int mid = (low + high)/2; // we look for the middle of the array in order to the partition
    	int[] S1 = Arrays.copyOfRange(arr, low, mid); // we copy one half to S1 and one half to S2
    	int[] S2 = Arrays.copyOfRange(arr, mid, high);
    	mergesort(0, S1.length, S1); // we Sort one side and the sort the other
    	mergesort(0, S2.length, S2);
    	merge(S1, S2, arr); // After being sorted we merge both sides together in sorted fashion
    }
    
    private static void merge(int[] S1, int[] S2, int[] arr) {
    	int s1Counter = 0;
    	int s2Counter = 0;
    	int indexToPlace = 0;
    	while(s1Counter < S1.length && s2Counter < S2.length) { // we merge by placing the smaller element first from each list
    		if(S1[s1Counter] <= S2[s2Counter]) { // if the smallest element is in S1 we place it in arr and move the pointer forward
    			arr[indexToPlace] = S1[s1Counter];
    			indexToPlace++;
    			s1Counter++;
    		}else { // if smalles element is in S2 we place it in arr and move the pointer forward
    			arr[indexToPlace] = S2[s2Counter];
    			indexToPlace++;
    			s2Counter++;
    		}
    	}
    	// at the end one list will be empty and the other not
    	while(s1Counter < S1.length) { // if S1 is not empty still, we place all remaining elements in arr
    		arr[indexToPlace] = S1[s1Counter];
    		s1Counter++; indexToPlace++;
    	}
    	while(s2Counter < S1.length) { // if S2 is not empty still, we place aall remaining elements in arr
    		arr[indexToPlace] = S2[s2Counter++];
    		indexToPlace++;
    	}	
    }
    
       
	public static void main(String[] args) {
		int[] test = { 100, 86, 44, 21, 102, 88, 99, 55, 88, 120, 100, 101, 0, -10, 66, 88, -49, -10000, 0, 66, 10000000, 88, -1000000000};
		int[] S1 = Arrays.copyOfRange(test, 0, 2);
		mergesort(test);
		System.out.println(Arrays.toString(test));
	}
	
}
