package AlgorithmAnalysis;

import java.util.Stack;

public class ExamTests {
	public static void main(String[] args) {
		Stack<Integer> s = new Stack<Integer>();
		for(int i = 1; i <= 7; i++) {
			s.push(i);
		}
		
		for(int i = 0; i < s.size(); i ++) {
			System.out.println(s.pop());
		}
	}
	
}
