package Exam3Tinkering;

import Queue.ArrayQueue;
import Queue.Queue;

public class PracticeProblems {

	
	public static Queue<Integer> ioIndexes(int n) {
		Queue<Integer> q = new ArrayQueue<Integer>();
		ioIndexes(n, q, 0);
		return q;
	}
	
	private static void ioIndexes(int n, Queue<Integer> q, int counter) {
		if(counter >= n) {
			return;
		}
		if(counter*2 +1 < n) {
			ioIndexes(n, q, counter*2+1);
		}
		q.enqueue(counter);
		if(counter*2+2 < n) {
			ioIndexes(n, q, counter*2+2);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue<Integer> q = ioIndexes(7);
		while(!q.isEmpty()) {
			System.out.println(q.dequeue());
		}
	}

}
