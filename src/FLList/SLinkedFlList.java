package FLList;

public class SLinkedFlList <E> implements FLList<E>, Cloneable {
	private Node<E> first;
	private Node<E>last = null;
	private int size;
	
	private static class Node<E>{
		private E element;
		private Node<E> next;
		
		public Node() {
			element = null;
			next = null;
		}
		
		public Node( E ets, Node<E> nts) {
			element = ets;
			next = nts;
		}
		
		public Node(E ets) {
			this(ets, null);
		}
		
		public E getElement() {
			return element;
		}
		
		public Node<E> getNext(){
			return next;
		}
		
		public void setNext(Node<E> newer) {
			next = newer;
		}
		
		public E setElement(E newer_element) {
			E etr = element;
			element = newer_element;
			return etr;
		}
		
	}
	
	public SLinkedFlList() {
		size = 0;
		first = null;
		last = null;
	}
		
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	private Node<E> findBeforeLastNode() {
		Node<E> p = first;
		int counter = 0;
		while(counter < size) { // Find Node before Last
			if (p.getNext() == last) {
				break;
			}
			counter ++;
			p = p.getNext();
		}
		return p;
	}
	
	@Override
	public E removeLast() {
		if(size == 0) {
			return null;
		}else if(size == 1) {
			E etr = first.getElement();
			first = null;
			last = null;
			size --;
			return etr;
		}else {
			Node<E> p = findBeforeLastNode();
			E etr = last.getElement(); // Save reference to the element stored in last
			last = p;
			p.setNext(first); // Change reference to the first to achieve circular implementation
			size --; 
			return  etr; // Return Reference to the removed element
		}
	}

	@Override
	public E removeFirst() {
		// TODO Auto-generated method stub
		if( size == 0 ) {
			return null;
		}
		else if ( size == 1) {
			E etr = first.getElement(); first = null; last = null;
			size--;
			return etr;
		}
		else {
			E etr = first.getElement();
			first = first.getNext();
			last.setNext(first);
			size--;
			return etr;
		}
	}

	@Override
	public void addLast(E element) {
		// TODO Auto-generated method stub
		if(size == 0) {
			Node<E> newNode = new Node<E>(element);
			first = newNode; last = newNode;
			last.setNext(first); first.setNext(last);
			size++;
		}
		else {
			Node<E> newerNode = new Node<E>(element);
			newerNode.setNext(last.getNext());
			last.setNext(newerNode);
			last = newerNode;
			size++;
		}
	}

	@Override
	public void addFirst(E element) {
		// TODO Auto-generated method stub
		if(size == 0) {
			Node<E> newerNode = new Node<E>(element);
			first = newerNode; last = newerNode;
			first.setNext(last); last.setNext(first);
			size ++;
		}else {
			Node<E> newerNode = new Node<E>(element);
			newerNode.setNext(first);
			first  = newerNode;
			last.setNext(first);
			size++;
		}
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		if(size == 0)return null;
		return first.getElement();
		
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		if(size == 0)return null;
		return last.getElement();
	}
	
	@Override
	public String toString() {
		if(size == 0) return null;
		int counter = 0; 
		Node<E> p = first;
		String tr = "";
		while(counter < size) {
			tr+= String.valueOf(counter) + ":" + p.getElement() + "\n";
			p = p.getNext();
			counter ++;
		}
		return tr;
	}
	
	
	@Override
	public Object clone() {
		SLinkedFlList<E> tr = new SLinkedFlList<E>();
		if(isEmpty()) return tr;
		Node<E> p = first;
		int counter = 0;
		while(counter < size) {
			tr.addLast(p.getElement());
			counter ++ ;
			p = p.getNext();
		}
		return tr;
	}
	
}
