package FLList;

public class FLListTester {
	public static void main (String[] args) {
		FLList <Integer> tester = new SLinkedFlList<Integer>();
		tester.addFirst(5);
		tester.addFirst(7);
		tester.addFirst(9);
		tester.addFirst(6);
		tester.addFirst(4);
		tester.addFirst(3);
		tester.addLast(78);
		tester.addLast(56);
		tester.addLast(44);
		tester.addLast(33);
		tester.addLast(22);
		tester.removeFirst();
//		tester.removeFist();
		tester.removeFirst();
		tester.removeLast();
		tester.addLast(3);
		tester.removeLast();
		tester.removeFirst();
		tester.removeLast();
		tester.removeFirst();
		StaticMethodsFLlist.FLRotate(tester);
		FLList<Integer> tester2 = (FLList<Integer>) tester.clone();
//		StaticMethodsFLlist.FLRotate(tester2);
		
		System.out.println(tester2);
		System.out.println(tester);
	}
}
