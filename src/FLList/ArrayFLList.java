package FLList;

public class ArrayFLList <E> implements FLList<E>, Cloneable {
	
	private static final int INIT_CAP = 10;
	private int first;
	private int size;
	private E[] elements;
	
	
	public ArrayFLList() {
		elements = (E[]) new Object[INIT_CAP];
		first = 0;
		size = 0;
	}
	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public E removeLast() {
		// TODO Auto-generated method stub
		if(elements.length/4 == size) {
			changeCapacity(size*2);
		}
		E etr = elements[(first + size -1)%elements.length];
		elements[(first + size -1)%elements.length] = null;
		size--;
		return etr;
	}

	@Override
	public E removeFirst() {
		// TODO Auto-generated method stub
		E etr = first();
		elements[first] = null;
		first = (first + 1)%elements.length;
		size--;
		return etr;
	}

	@Override
	public void addLast(E element) {
		// TODO Auto-generated method stub
		if(size == elements.length) {
			changeCapacity(elements.length*2);
		}if(size == 0) {
			elements[first] = element;
			size++;
		}
		elements[(first + size)%elements.length ] = element;
		size++;
	
	}

	@Override
	public void addFirst(E element) {
		// TODO Auto-generated method stub
		if(size == elements.length) {
			changeCapacity(elements.length*2);
		}
		if(size == 0) {
			elements[first] = element;
			size++;
		}else {
			elements[(first + elements.length - 1)%elements.length ] = element;
			first = (first + elements.length - 1)%elements.length ;
			size++;
		}
			
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		return elements[first];
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		return elements[(first+size-1)%elements.length];
	}
	
	private void changeCapacity(int cap) {
	
		E[] toSet = (E[]) new Object[cap] ;
		for(int i = 0; i<size; i++) {
			toSet[i] = elements[(first+i)%elements.length];
		}
		elements = toSet.clone();
		first = 0;
	}
	
	public String toString() {
			String toReturn = "";
			for(int i = 0; i < elements.length; i++) {
				toReturn += String.valueOf(i) + ": " + String.valueOf(elements[i]) + "\n";
			}
			return toReturn;
	}

	public Object clone(){
		ArrayFLList<E> tr = new ArrayFLList<E>();
		if(isEmpty()) return tr;
		for( int i = 0; i < size; i++) {
			tr.addLast(elements[(first+i)%elements.length]);
		}
		return tr;
	}
}
