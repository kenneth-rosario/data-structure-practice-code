package FLList;


public interface FLList <E> {
	public boolean isEmpty();
	public int size();
	public E removeLast();
	public E removeFirst();
	public void addLast(E element);
	public void addFirst(E element);
	public E first();
	public E last();
	public Object clone();
}
