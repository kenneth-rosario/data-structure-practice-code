package PositionalList;

import java.util.Iterator;

public interface PositionalList<E> extends Iterable<E>{
	Position<E> addFirst(E p);
	Position<E> addLast(E p);
	Position<E> after(Position<E> p);
	Position<E> before(Position<E> p);
	Position<E> first();
	Position<E> last();
	boolean isEmpty();
	int size();
	Position<E> addBefore(Position<E> p, E e);
	Position<E> addAfter(Position<E> p, E e);
	E set(Position<E> p, E e);
	E remove(Position<E> p);
}
