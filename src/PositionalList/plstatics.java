package PositionalList;

import java.util.Comparator;

public class plstatics {
	
	public static void insertionSort(PositionalList<Integer> p) {
		
		Position<Integer> cursor = p.first();
		while(cursor != null) {
			Integer key = cursor.getElement();
			Position<Integer> j = p.before(cursor);
			while (j != null ) {
				if(key >= j.getElement() ) {
					break;
				}
				p.set(p.after(j), j.getElement());
				j = p.before(j);
			}
			p.set(p.after(j), key);
			cursor = p.after(cursor);
		}
		
	}
	
	public static void selectionSort(PositionalList<Integer> p) {
		
		Position<Integer> cursor = p.first();
		while(cursor != null) {
			Position<Integer> min = cursor;
			Position<Integer>j = p.after(cursor);
			while(j.getElement() != null) {
				if(j.getElement() < min.getElement()) {
					min = j;
				}
				j = p.after(j);
			}
			Integer temp = min.getElement();
			p.set(min, cursor.getElement());
			p.set(cursor, temp);
			cursor = p.after(cursor);
		}
		
	}
	
}
