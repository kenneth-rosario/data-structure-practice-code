package PositionalList;

import java.util.Iterator;

public class DoublyLinkedPositionalList<E> implements PositionalList<E> {
	
	Node<E> header;
	Node<E> trailer;
	int size;
	
	private static class Node<E> implements Position<E>{
		
		private Node<E> next;
		private Node<E> prev;
		private E element;
		
		public Node(E e, Node<E> p, Node<E> n) {
			element = e;
			prev = p;
			next = n;
		}
		
		@Override
		public E getElement() {
			// TODO Auto-generated method stub
			return element;
		}
		
		public void setElement(E e) {
			element = e;
		}
		
		public Node<E> getNext(){
			return next;
		}
		
		public void setNext(Node<E> n) {
			next = n;
		}
		
		public Node<E> getPrev() {
			return prev;
		}
		
		public void setPrev(Node<E> p) {
			prev = p;
		}
		
	}
	
	public DoublyLinkedPositionalList() {
		header = new Node<E>(null, null, null);
		trailer = new Node<E>(null, header, null);
		header.setNext(trailer);
		size = 0;
	}
	
	private void validate(Position<E> p) {
		
		if(!(p instanceof Node)) {
			throw new IllegalArgumentException("Position Not Valid");
		}
	
	}
	
	@Override
	public Position<E> addFirst(E p) {
		// TODO Auto-generated method stub
		Node<E> nn = new Node<E>(p, header, header.getNext());
		header.setNext(nn); nn.getNext().setPrev(nn);
		size++;
		return nn;
	}

	@Override
	public Position<E> addLast(E p) {
		// TODO Auto-generated method stub
		Node<E> nn = new Node<E>(p, trailer.getPrev(), trailer);
		trailer.setPrev(nn); nn.getPrev().setNext(nn); size++;
		return nn;
	}

	@Override
	public Position<E> after(Position<E> p) {
		// TODO Auto-generated method stub
		validate(p);
		if( ((Node<E>)p).getNext() == trailer ) {
			return null;
		}
		return ((Node<E>)p).getNext();
	}

	@Override
	public Position<E> before(Position<E> p) {
		// TODO Auto-generated method stub
		validate(p);
		if( ((Node<E>)p).getPrev() == header ) {
			return null;
		}
		return ((Node<E>)p).getPrev();
		
	}

	@Override
	public Position<E> first() {
		// TODO Auto-generated method stub
		return header.getNext();
	}

	@Override
	public Position<E> last() {
		// TODO Auto-generated method stub
		return trailer.getPrev();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public Position<E> addBefore(Position<E> p, E e) {
		// TODO Auto-generated method stub
		validate(p);
		Node<E> np = (Node<E>) p;
		Node<E> toAddBefore = new Node<E>(e, np.getPrev(), np);
		np.setPrev(toAddBefore); toAddBefore.getPrev().setNext(toAddBefore);
		size++;
		return toAddBefore;
		
	}

	@Override
	public Position<E> addAfter(Position<E> p, E e) {
		// TODO Auto-generated method stub
		validate(p);
		Node<E> theP = (Node<E>) p;
		Node<E> newNode = new Node<E>(e, theP, theP.getNext() );
		theP.setNext(newNode); newNode.getNext().setPrev(newNode);
		size++;
		return newNode;
		
	}

	@Override
	public E set(Position<E> p, E e) {
		// TODO Auto-generated method stub
		validate(p);
		E tr = p.getElement();
		((Node<E>) p).setElement(e);
		return tr;
	}

	@Override
	public E remove(Position<E> p) {
		// TODO Auto-generated method stu
		validate(p);
		Node<E> newP = (Node<E>)p;
		E etr = newP.getElement();
		newP.getPrev().setNext(newP.getNext());
		newP.getNext().setPrev(newP.getPrev());
		newP.setNext(null);newP.setPrev(null);
		size--;
		return etr;
	}

	// Iterators and Iterable functionalities
	
	@Override
	public Iterator<E> iterator() {
		
		return new Iterator<E>() {
			private Iterator<Position<E>>pIterator = new PositionIterator(); 
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return pIterator.hasNext();
			}

			@Override
			public E next() {
				// TODO Auto-generated method stub
				return pIterator.next().getElement();
			}
			
			@Override
			public void remove() {
				pIterator.remove();
			}
			
		};
		
	}
	
	public Iterable<Position<E>> positions(){
		return new Iterable<Position<E>>() {
			@Override
			public Iterator<Position<E>> iterator() {
				return new PositionIterator();
			}
		};
	}
	
	private class PositionIterator implements Iterator<Position<E>>{
		private Node<E> current = header.getNext();
		private Position<E> recent = null;
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != trailer;
		}

		@Override
		public Position<E> next() {
			// TODO Auto-generated method stub
			if(!hasNext()) throw new IllegalStateException("No more next");
			Position<E> toReturn = current;
			recent = toReturn;
			current = current.getNext();
			return toReturn;
		}
		
		@Override
		public void remove() {
			if(recent == null) {
				throw new IllegalStateException("Not a valid remove call");
			}
			DoublyLinkedPositionalList.this.remove(recent);
			recent = null;
		}
			
	}
	

}

