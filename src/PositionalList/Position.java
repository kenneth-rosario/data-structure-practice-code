package PositionalList;
/*
 * Encapsulation of Node 
 * Purpose is to return a Node with only a getElement method as a Position
 */
public interface Position<E> {
	E getElement();
}
