package EXAM1Tinkering;

public class Main {

	private static class MyClass1 {
		private int a; 
		  public int b; 
		  public static int bs = 2; 
		  public MyClass1(){ this(7); b = 5;} 
		  public MyClass1(int x) { a = x; }
		  public int s(){ return a + b + t(); } 
		  protected int t(){ return a; } 

	}
	
	private static class MyClass2 extends MyClass1{
		private int c;
		  public MyClass2(int x) { c = x; }
		  public MyClass2() {super(3); b =3; c =3;}
		  protected int t(){ return c; } 

	}
	
	private static class MyClass3 extends MyClass1{
		private int d; 
		  public MyClass3(){ d = 2; }
		  public int s(){ return d + t(); } 

	}
	


//public static int what(int n) { 
//   int s = 0; 
//   for (int i=1; i<=n; i++) {  
//       for (int j=0; j<i; j++)
//           s++; 
//       s++;  
//   } 
//   return s; 
//}
public static int what(int n) { 
	   int s = 0; 
	   int[] r = new int[n]; 
	   for (int i=0; i<n; i++) 
	       r[i] = i+2; 
	   for (int i=1; i<=n; i++)
	       s = s + r[i-1];  
	   return s; 
	}


	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(what(2));
	}

}
