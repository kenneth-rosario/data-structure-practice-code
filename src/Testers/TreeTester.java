package Testers;

import java.util.ArrayList;
import java.util.Iterator;

import Queue.ArrayQueue;
import Queue.Queue;
import Stacks.ArrayStack;
import Trees.LinkedGeneralTree;
import Trees.Position;
import Trees.Tree;
import Trees.TreeIterator;

public class TreeTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			LinkedGeneralTree<String> t = new LinkedGeneralTree<>(); 
			
			// add nodes and data to the tree
			Trees.Position<String> p = t.addRoot("ROOT"); 	
			t.addChild(p, "Rosa"); 
			p = t.addChild(p, "Maria"); 
			Trees.Position<String> p1 = t.addChild(p, "Juana"); 
			p1 = t.addChild(p1, "Lola"); 
			t.addChild(p1, "Pepote"); 
			p1 = t.addChild(p1, "Manolo"); 
			p1=t.addChild(p1, "Eligio"); 
			t.addChild(p1, "Eda"); 
			t.addChild(p1, "Deborah"); 
			p1 = t.addChild(p, "Pergamino"); 
			t.addChild(p, "Bienvenido");
			t.addChild(p1, "Manolin"); 
			t.addChild(p1, "Juaniquillo"); 
			t.addChild(p1, "Andres"); 
			p1 = t.addChild(t.root(), "Mariola");
			p = p1; 
			p1 = t.addChild(p1, "Leslo"); 
			p1 = t.addChild(p1, "Papin"); 
			p1 = t.addChild(p1, "Ana"); 
			t.addChild(p, "Elegancia"); 
			t.display();
			
			LinkedGeneralTree<Integer> tester1 = new LinkedGeneralTree<>();
			Position<Integer> c1 = tester1.addRoot(2);
			Position<Integer>c5  = tester1.addChild(c1, 5);
			tester1.addChild(c5, 3); tester1.addChild(c5, 7);
			tester1.addChild(c1, 1);
			Position<Integer> c4 = tester1.addChild(c1, 4);
			tester1.addChild(c4, 8);
			tester1.addChild(c4, 0);
			tester1.addChild(c4, 6);
		
			
			
			posOrderTraversal(t);
			System.out.println("With Stack : \n");
			preOrder(t);
			ArrayList<String> t1 = breadthFirstSearch(t);
			for(String s: t1) {
				System.out.println(s);
			}
			System.out.println(" ");
			for(Integer i: theValues(tester1)) {
				System.out.println(i);
			}
			System.out.println("\n");
			Iterator<Integer> tIt = new TreeIterator<Integer>(tester1);
			while(tIt.hasNext()) {
				System.out.println(tIt.next());
			}
			
	}
	
	private static <E> void preOrder(Tree<E> t) {
		ArrayStack<Position<E>> s = new ArrayStack<>();
		s.push(t.root());
		while(s.isEmpty()) {
			System.out.println(s.top().getElement());
			for(Position<E> p : t.children(s.pop())) {
				s.push(p);
			}
		}
	}
	
	private static <E> void posOrderTraversal(Tree<E> t) {
		recPos(t, t.root());
	}
	
	private static <E> void recPos(Tree<E> t, Position<E> p) {
		System.out.println(p.getElement());
		for( Position<E> pc : t.children(p)) {
			recPos(t, pc);
		}
	}
	
	private static <E> ArrayList<E> breadthFirstSearch(Tree<E> t) {
		ArrayList<E> toFill = new ArrayList<E>();
		Queue<Position<E>> q = new ArrayQueue<Position<E>>();
		recBreadthFirst(q, toFill, t, t.root());
		return toFill;
	}
	
	private static <E> void recBreadthFirst(Queue<Position<E>> q, ArrayList<E> toFill, Tree<E> t, Position<E> root) {
		toFill.add(root.getElement());
		for(Position<E> p: t.children(root)) {
			q.enqueue(p);
		}
		while (!q.isEmpty()) {
			recBreadthFirst(q, toFill, t, q.dequeue());
		}
	}

	
    public static Iterable<Integer> theValues(Tree<Integer> t) { 
        ArrayList<Integer> result = new ArrayList<>(); 
        if (!t.isEmpty()) 
           genValues(t, t.root(), 0, result); 
        return result;
    } 
    
    private static void genValues(Tree<Integer> t, Position<Integer> r, Integer pathValue, 
            ArrayList<Integer> result) {
    	// Visit Before
    	Integer newValue = pathValue + r.getElement();
    	if(t.isExternal(r)) {
    		result.add(newValue);
    	}
    	// Then visit childs
    	for(Position<Integer> p: t.children(r)) {
    		genValues(t, p, newValue, result);
    	}

    }

}
