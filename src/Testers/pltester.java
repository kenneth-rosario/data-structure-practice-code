package Testers;
import PositionalList.PositionalList;
import PositionalList.DoublyLinkedPositionalList;
import PositionalList.Position;
import PositionalList.plstatics;
public class pltester {
	
	
	public static void main(String[] args) {
		PositionalList<Integer> tester = new DoublyLinkedPositionalList<Integer>();
		Position<Integer> p1 = tester.addFirst(3);
		System.out.println(tester.first().getElement());
		Position<Integer> p2 = tester.addBefore(p1, 4);
		System.out.println(tester.first().getElement());
		Position<Integer> p3 = tester.addAfter(p2, 5);
		tester.addAfter(p3, 9);
		tester.addBefore(p2, 90);
		tester.addBefore(p3, 100);
		Position<Integer> cursor = tester.first();
		int counter = 0;
		while(cursor != null) {
			System.out.println(counter+":"+cursor.getElement());
			cursor = tester.after(cursor); counter++;
		}
		System.out.println(tester.before(p1).getElement());
		cursor = tester.first();
		counter = 0;
		while(cursor !=null) {
			System.out.println(counter+":"+cursor.getElement());
			cursor = tester.after(cursor); counter++;
		}
		
		for(Integer e: tester) {
			System.out.println(e);
		}
	}
	
}
