package Queue;

public class SLLQueue<E> implements Queue<E>{
	
	private static class Node<E>{
		private E element;
		private Node<E> next;
		
		public Node() {
			element = null;
			next = null;
		}
		
		public Node( E ets, Node<E> nts) {
			element = ets;
			next = nts;
		}
		
		public Node(E ets) {
			this(ets, null);
		}
		
		public E getElement() {
			return element;
		}
		
		public Node<E> getNext(){
			return next;
		}
		
		public void setNext(Node<E> newer) {
			next = newer;
		}
		
		public E setElement(E newer_element) {
			E etr = element;
			element = newer_element;
			return etr;
		}
		
	}
	
	Node<E> first = null;
	Node<E> last = null;
	int size = 0;
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}
	@Override
	public E first() {
		// TODO Auto-generated method stub
		if (isEmpty()) return null;
		return  first.getElement();
	}
	@Override
	public void enqueue(E element) {
		// TODO Auto-generated method stub
		if(isEmpty()) {
			Node<E> toEnqueue = new Node<E>(element, null);
			last = first = toEnqueue; size++;
			return;
		}
		Node<E> toEnqueue = new Node<E>(element, null);
		last.setNext(toEnqueue); last = toEnqueue; size++;
	}
	@Override
	public E dequeue() {
		// TODO Auto-generated method stub
		if(isEmpty()) return null;
		E etr = first.getElement();
		first = first.getNext();
		size--;
		return etr;
	}
	
	
	
}
