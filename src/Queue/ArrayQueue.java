package Queue;


public class ArrayQueue<E> implements Queue<E>{
	private final static int INIT_CAP = 20;
	E[] arr = (E[]) new Object[INIT_CAP];
	int first = 0;
	int size = 0;
	E last = null;
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		if(isEmpty()) {
			return null;
		}
		return arr[first];
	}

	private void changeCapacity(int n) {
		if(n < INIT_CAP) {
			return;
		}
		E[] newArr = (E[]) new Object[n];
		for(int i = 0; i < size; i++) {
			newArr[i] = arr[(first + i)%arr.length];
		}
		first = 0;
	}
	
	@Override
	public void enqueue(E element) {
		// TODO Auto-generated method stub
		if(arr.length == size) {
			changeCapacity(arr.length * 2);
		}else if(arr.length/4 == size ) {
			changeCapacity(arr.length/2);
		}
		if(size == 0) {
			first = 0;
			arr[first] = element; size++;
			return;
		}
		arr[(first + size)%arr.length] = element;
		last = element; size++;
		
	}

	@Override
	public E dequeue() {
		// TODO Auto-generated method stub
		if(arr.length/4 == size ) {
			changeCapacity(arr.length/2);
		}
		E etr = arr[first]; size--;
		first = (first + 1)%arr.length;
		return etr;
	}

}
