package Stacks;

public class BlockStack extends ArrayStack<Integer>{

    /** Pushes a new chunk on top of the stack, whose elements are the elements of the array given as explicit parameter. 
          All elements in the array are added as part of a �single� chunk. 
          @param  arr the array whose elements are added to the stack, from 0 to arr.length-1. 
    **/ 
    public void push(Integer[] arr) {
    	for(int i = 0; i < arr.length; i++) {
    		super.push(arr[i]);
    	}
    	super.push(arr.length);
    }

    /** Works as the normal push. But the element to be added to the stack is added as a chunk of size 1. 
          @param e the element to be added to the stack. 
    **/
    public void push(Integer e) {
    	super.push(e);
    	super.push(1);
    }

    /** Pops all the elements from the block on top of the stack. Elements are returned as content of an array. 
          @return An array of the elements on the block on top of the stack; of null if the stack is empty. 
    **/
    public Integer[] popBlock() {
    	Integer BlockLength = super.pop();
    	Integer[] tr = new Integer[BlockLength];
    	for(int i = BlockLength-1; i >= 0; i--) {
    		tr[i] = super.pop();
    	}
    	return tr;
    }

    /** Works as the normal pop operation on stacks, but requires that the block on the top of the stack has
          size == 1. If block on top is not of size 1, the announced exception is thrown. 
          @return If valid to do so, returns the single element from the block of size 1 on top of the stack. It returns
          null if the stack is empty.
          @throws IllegalStateException Whenever the stack is not empty and the block at the top is of size > 1. 
    **/ 
    public Integer pop() throws IllegalStateException {
    	int topLength = super.top();
    	if(topLength != 1 ) {
    		throw new IllegalStateException("Top block length is not 1");
    	}
    	super.pop(); 
    	return super.pop();
    } 
}
