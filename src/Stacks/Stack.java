package Stacks;

public interface Stack<E> {
	
	E pop();
	
	
	E top();
	
	int size();
	
	boolean isEmpty();

	void push(E e);
	
}
