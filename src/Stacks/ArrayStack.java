package Stacks;

public class ArrayStack<E> implements Stack<E>{
	E[] elements;
	int size = 0;
	public static final int INITCAP = 10;
	
	public ArrayStack() {
		elements = (E[]) new Object[INITCAP]; 
	}
	
	@Override
	public E pop() {
		// TODO Auto-generated method stub
		if(isEmpty()) return null;
		E temp = elements[size -1];
		elements[size-1] = null;
		size--;
		return temp;
		
	}

	private void changeCapacity(int number) {
		E[] newElements = (E[])new Object[number];
		for(int i = elements.length-1; i >= 0; i --) {
			newElements[i] = elements[i];
		}
		elements = newElements; 
	}
	
	public void push(E e) {
		// TODO Auto-generated method stub
		if(size == elements.length) {
			changeCapacity(elements.length * 2);
		}
		elements[size] = e;
		size++;
		
	}

	@Override
	public E top() {
		// TODO Auto-generated method stub
		return elements[size-1];
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	
}
